package com.conygre.training.tradesimulator.sim;

import static org.mockito.Mockito.when;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeState;
import com.conygre.training.tradesimulator.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TradeSimMockingTest {
    
    @Autowired
    private TradeSim tradeSim;

    @MockBean
    private TradeMongoDao mockTradeDao;
    
    @Test
    public void test_tradeSim_save(){
        Trade testTrade = new Trade();

        testTrade.setDateCreated((java.util.Date) Date(System.currentTimeMillis()));
        testTrade.setRequestedPrice(10);
        testTrade.setStockQuantity(500);
        testTrade.setStockTicker("AMZN");
        testTrade.setTradeState(TradeState.CREATED);
        testTrade.setTradeType(TradeType.BUY);

        // tell mockRepo that employeeService is going to call save()
        // when it does return the testEmployee object
        when(mockTradeDao.save(testTrade)).thenReturn(testTrade);

        //tradeSim.findTradesForProcessing( testTrade);
    } 
    
    private Object Date(long currentTimeMillis) {
        return null;
    }

   
}
