package com.conygre.training.tradesimulator.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Main Trade class with variables, see below classes for trade states and trade type enums 
 * @see TradeState
 * @see TradeType
 */
@Document
public class Trade {

    @Id
    private String id;
    private Date dateCreated = new Date(System.currentTimeMillis());
    private String stockTicker;
    private int stockQuantity;
    private double requestedPrice;
    private TradeType tradeType = TradeType.BUY;
    private TradeState tradeState = TradeState.CREATED;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    };

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public TradeState getTradeState() {
        return this.tradeState;
    }

    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

    public Object getStockTicker() {
        return this.stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public int getStockQuantity() {
        return this.stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public TradeType getTradeType() {
        return this.tradeType;
    }

    public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}

    public double getRequestedPrice() {
        return this.requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

}
