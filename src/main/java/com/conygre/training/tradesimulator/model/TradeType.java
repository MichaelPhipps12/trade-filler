package com.conygre.training.tradesimulator.model;

/**
 * TradeType enum may be: BUY or SELL
 */
public enum TradeType {
    BUY("BUY"),
    SELL("SELL");

    private String tradeType;

    private TradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeType() {
        return this.tradeType;
    } 
}
